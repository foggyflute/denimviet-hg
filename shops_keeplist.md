BOUTIQUE

7 Men In Blue
Mr.Sanghyu Na
Boutique - Denim, Clothes, Accessories

LOCALBRAND

8 Copper Denim
Mr. Tú Phạm
Local brand - Denim, Clothes
Boutique - Accessories

9 Thợ (haianhcongnhan)
Mr. Cao Mạnh Duy
Local brand - RTW Boots, Clothes
Boutique - Clothes, Accessories
Used Goods Store - Boots

6 Amauri Menswear
Mr. Hoàng Tuấn Long
Local brand - RTW Clothes

10 Mansio Jeans
Mr. Võ Văn Sĩ
Local brand - Denim
Manufacturer - Denim

11 Crowd Wolf Leather
Mr. Chiến Bùi
Local brand - Handmade wallet, belt

12 The Labour Supply Co.
Mr.Tô Thành
Local Brand - Pomade

SECONDHAND

13 Akito Shop
Mr. Trung Lê
Used Goods Store - Boots & Denim
Local Brand - Boots

14 Denimister
Mr. Nhân Nguyễn
Used Goods Store - Boots & Denim

15 The Old Tags
Mr. Đào Đức
Used Goods Store - Denim

16 89 warehouse
Mr. Tăng Đức Trí
Used Goods Store - Shoes & Boots
Local brand - RTW Shoes & Boots

17 MOC Thrift Shop
Mr. Nam Đàm
Used Goods - Clothes, Denim, Shoes

18 Tạp Hóa 22
Mr. Minh Tân
Used Goods Store - Denim, Clothes & Boots

19 Gallantry Shoes Store
Mr. Khoa Phạm
Used Goods Store - Shoes & Boots

<!-- Kent Nguyen Authentic
Mr. Kent Nguyen
Boutique - Denim, Clothes, Accessories
Used Goods Store - Shoes & Boots -->

'SLP' Aesthetic 

20 TCB - The call boy
Mr. Nguyễn Đăng Trung
Local brand - RTW Chelsea, Zip Boots, Combat Boots, Contemporary Shoes.

21 Tamorshoes
Mr. Ngô Anh
Local brand - RTW Chelsea, Zip Boots, Contemporary Shoes.

22 Go Grunge 90s
Mr. Lê Vĩnh Đạt
Local brand - Chelsea, Zip Boots

BESPOKE

23 Antonio De Torres
Mr. Luis Antonio Torres III
Bespoke service - Suits & Clothes, Jackets, Leather Jackets, Denim
RTW Shoes, Boots, Clothes & Accessories

24 Phạm Tailor
Mr. Trí Phạm
Bespoke service - Suits & Clothes, Jackets, Denim
RTW Shoes & Accessories

25 Carlo Phạm
Mr. Hoàng Đình Đức
Bespoke service - Suits & Clothes, Jackets

26 Veneta Bespoke
Mr. Minh Quân
Bespoke service - Suits & Clothes, Jackets