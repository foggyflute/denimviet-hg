---
type: page
weight: 4002
title: CNES Shoemaker
subtitle: Thương hiệu lớn - Established brand
filters: [Giày và bốt, Made to measure, Made to order, cửa hàng HN, cửa hàng HCM, cửa hàng Tỉnh]
---

{{< bimg "cover.jpg" >}}
___

Thương hiệu bán lẻ CNES tuy còn mới nhưng họ có kinh nghiệm 20 năm gia công sản xuất. CNES khẳng định họ là nhà sản xuất giày thủ công goodyear-welt lớn nhất châu Á và là đối tác nhập da lớn nhất của Hermès trên toàn Đông Nam Á tại thời điểm hiện tại. Ngoài giày goodyear, các mặt hàng phụ kiện da và giày blake (McKay) của CNES cũng rất phong phú và có mức giá cạnh tranh.

<!--more-->

**Địa chỉ:**

* CNES Hà Nội - 71 Tuệ Tĩnh, Q.Hai Bà Trưng Hà Nội, 024-6688-8262
* CNES Hồ Chí Minh - 43A Ba Tháng Hai, Q.10 tp.HCM, 028-2246-3366
* Sense Of Beauty - Lầu 3 Takashimaya, 92-94 Nam Kỳ Khởi Nghĩa, Q.1 tp.HCM, 028-5431-4314
* CNES Bespoke - 206 Lê Lai, P. Phạm Ngũ Lão, Q.1 tp.HCM 
* CNES Hải Dương - 9 Đại lộ Hồ Chí Minh, P. Nguyễn Trãi, TP. Hải Dương, 094-213-5599
* CNES Nha Trang - 12A Lê Lợi, P. Xương Huân, TP. Nha Trang, 098-388-6738
* CNES Quảng Ngãi - 434 Quang Trung, P. Nguyễn Nghiêm, TP. Quảng Ngãi, 0122-991-8588
* Xứ Đàng Trong - 09 Nguyễn Thái Học, Hội An, 0903594085

**Online:**

* https://cnes.com.vn/
* https://www.facebook.com/cnes.com.vn/

**Đại Diện:** Mr. Minh Dũng

**Phân khúc:** Giày & Bốt có sẵn và MTO từ 1.600.000đ đến \~20.000.000đ, phụ kiện từ \~250.000đ.

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW shoes & soots
* Giày tây, bốt đóng theo yêu cầu - MTO shoes & boots
* Giày tây, bốt đóng theo số đo - MTM shoes & boots
* Phụ kiện da - Leather accessories