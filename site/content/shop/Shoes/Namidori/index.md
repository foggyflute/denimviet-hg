---
type: page
weight: 4004
title: Namidori
subtitle: Thương hiệu nội - Local brand
filters: [Giày và bốt, cửa hàng HN, cửa hàng HCM]
---

{{< bimg "cover.jpg" >}}
___

Ra đời từ 2014 với thị trường ban đầu là xuất khẩu Nhật Bản, Namidori cũng là một trong số ít các thương hiệu đầu tiên tại VN bán ra có sử dụng cấu trúc GYW. Namidori xuất phát từ chữ midori trong tiếng Nhật, nghĩa là “màu xanh” - tượng trưng cho sự tự nhiên và thân thiện của sản phẩm.

<!--more-->
 
**Địa chỉ:**

* 105 Phạm Ngũ Lão, Q1, HCM
* 85 Nguyễn Hữu Huân,Q. Hoàn Kiếm, HN

**Online:**

* https://www.namidori.com
* https://www.facebook.com/namidorishoes

**Đại Diện:** Mr. Đào Ngọc Anh

**Phân khúc:**  Giày & Bốt có sẵn \~1.600.000đ đến 5.000.000đ, Phụ kiện, sản phẩm chăm sóc giày da từ \~150.000đ.

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW shoes & boots
* Giày tây, bốt đóng theo yêu cầu - MTO shoes & boots
* Sản phẩm chăm sóc đồ da - Leather care products
* Phụ kiện da - Leather accessories
* Phụ kiện thời trang nam - Menswear accessories