---
type: page
weight: 4005
title: Knar Shoemaker
subtitle: Thương hiệu nội - Local brand
filters: [Giày và bốt, cửa hàng HN]
---

{{< bimg "cover.jpg" >}}
___

Đây là nhãn hàng tiên phong đầu tiên của Việt Nam nhắm vào dòng sản phẩm workwear boots ngay từ khi thị trường này còn chưa định hình, không chỉ như vậy, các sản phẩm của Knar khá đa dạng và bao quát toàn bộ ngành hàng từ phân khúc thấp đến cao. Tất cả các mẫu boot của KNAR đều sử dụng cấu trúc đế khâu goodyear hoặc blake rapid.

Thương hiệu tập trung vào Service Boot, Engineer Boot, Work Boot, Combat Boot,... và tất cả những kiểu giày hầm hố mạnh mẽ. Với nền tảng cùng 1 nhà với CNES, Knar mới về thương hiệu vầ mẫu mã nhưng chất lượng thì không hề xa lạ người dùng.

<!--more-->

**Địa chỉ:**

* 17 ngõ 2 Tây Sơn, Q. Đống Đa, Hà Nội, 024-6686-6682

**Online:**

* https://knar.com.vn/
* https://www.facebook.com/knar.com.vn/

**Đại Diện:** Mr. Minh Dũng

**Phân khúc:** Giày & Bốt có sẵn và MTO \~2.800.000đ đến 25.000.000đ.

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW Shoes & Boots
* Giày tây, bốt đóng theo yêu cầu - MTO shoes & boots