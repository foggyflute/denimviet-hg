---
type: page
weight: 4003
title: Camos Classic
subtitle: Thương hiệu nội - Local brand
filters: [Giày và bốt, Phụ kiện, cửa hàng HN]
---

{{< bimg "cover.jpg" >}}
___

Ra đời từ 2017, thương hiệu nội Camos đã có sự mở rộng đáng kể về mặt hàng, trong đó nổi bật nhất là sản phẩm gọng kính cận/kính mát. Tuy nhiên sản phẩm giày tây vẫn chiếm vị trí quan trọng trong catalogue của thương hiệu và vẫn không ngừng cập nhập, nâng cấp về kiểu dáng và chất liệu.

<!--more-->

**Địa chỉ:**

* Số 02 - Trần Quốc Toản, Hoàn Kiếm Hà Nội, 038-959-8988

**Online:**

* https://www.facebook.com/pg/CamosClassic/

**Đại Diện:** Mr. Trương Công Thành

**Phân khúc:** Giày & Bốt có sẵn \~1.600.000đ đến 4.000.000đ, Phụ kiện, sản phẩm chăm sóc giày da từ \~150.000đ.

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW shoes & boots
* Sản phẩm chăm sóc đồ da - Leather care products
* Phụ kiện da - Leather accessories
* Phụ kiện thời trang nam - Menswear accessories