---
type: page
weight: 4001
title: Fugashin Saigon
subtitle: Thương hiệu lớn - Established brand
filters: [Giày và bốt, Bespoke, Made to measure, Made to order, cửa hàng HCM]
---

{{< bimg "cover.jpg" >}}
___

Fugashin - tên tiếng Việt là Thuận Buồm, là công ty chế tác giày da cao cấp liên doanh Việt Nam - Nhật Bản. Công ty thừa hưởng quy trình, kĩ thuật sản xuất và kiểm tra chất lượng gắt gao từ phía đối tác nhật bản và hiện đang gia công cho rất nhiều thương hiệu tên tuổi trên thế giới, kể cả dòng hàng hand-welt (gò-khâu đế tay) được coi là luxury trong ngành giày.

Đây là thương hiệu nổi tiếng nhất và cũng là thương hiệu giày duy nhất tại thời điểm hiện tại có dịch vụ bespoke đúng theo đẳng cấp quốc tế: bao gồm đo chân, làm last (phom giày riêng), đóng thử giày mẫu thử chân trước khi đóng chính thức sản phẩm giao cho khách.

<!--more-->

**Địa chỉ:**

* 38 Phan Văn Hớn, Tân Thới Nhất, q.12 tp.HCM, 028-388-333-22
* 15 Võ Văn Tần, p.6, quận 3 tp.HCM, 028-3930-6448

**Online:**

* http://fugashin-saigon.com
* https://facebook.com/Fugashinshoemaker

**Đại Diện:** Mr. Quách Lộc Thành

**Phân khúc:** Giày & Bốt có sẵn & MTO từ \~1.800.000đ đến 30.000.000đ, phụ kiện từ \~400.000đ. MTM từ 8.000.000đ, Bespoke giá liên hệ (tùy theo yêu cầu). Phụ kiện da \~từ 500.000đ

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW Shoes & Boots
* Giày tây, bốt đóng theo yêu cầu - MTO Shoes & Boots
* Giày tây, bốt đóng theo chân - Bespoke Shoes & Boots
* Phụ kiện da - Leather accessories