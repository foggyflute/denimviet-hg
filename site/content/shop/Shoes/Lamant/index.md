---
type: page
weight: 4006
title: L'amant Shoemaker.
subtitle: Thương hiệu nội - Local brand
filters: [Giày và bốt, Phụ kiện, cửa hàng HN]
---

{{< bimg "cover.jpg" >}}
___

Thành lập từ 2017, chuyên nhắm vào dòng hàng trung cấp giá dễ chịu cho dân công sở, L'amant sở hữu những thiết kế giày khá nhẹ nhàng đơn giản và điệu đà theo phong cách 'quý ông' với cấu trúc blake là chủ đạo. Ngoài 2 cửa hàng chính, L'amant còn có sản phẩm ở rất nhiều cửa hàng chuyên veston may đo khắp Hà Nội.

<!--more-->

**Địa chỉ:**

* 186 Xã Đàn 2, Đống Đa, Hà Nội, 0965-893-773
* 49 Cửa Nam, Hoàn Kiếm, Hà Nội

**Online:**

* www.lamant.store
* https://www.facebook.com/lamantshoe/

**Đại Diện:** Mr. Hoàng Tuấn Long

**Phân khúc:** Giày & Bốt có sẵn từ \~2.000.000đ.

**Sản Phẩm:**

* Giày tây, bốt đóng sẵn - RTW Shoes & Boots
* Phụ kiện da - Leather accessories
* Phụ kiện thời trang nam - Menswear accessories