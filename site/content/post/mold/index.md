---
title: "Xử lí nấm mốc trên giày da"
subtitle: "Đã bị rồi cứ tái đi tái lại."
date: 2017-11-02
draft: false
tags: ["Hướng Dẫn", "Giày và bốt"]
slug: "xu li nam moc"
image: "/post/mold/xu-li-nam-moc/cover.jpg"
---

{{< bimg "cover.jpg" >}}

Mốc (mold) nấm tồn tại bên trong các lỗ hổng của da (pore). Phần bông bông xốp có màu xám hoặc trắng bên trên chỉ là phần phát tán bào tử của nó thôi. Có lau sạch cỡ nào đi nữa nó cũng lại mọc lên, vì đó chỉ là cắt cỏ ở ngọn. Cho nên các lời khuyên lau đi rồi đánh xi bôi kem dưỡng saphir hay gì gì, hoàn toàn là vô ích trong việc diệt nấm mốc. Có khi con mốc còn cảm ơn đã cung cấp chất dinh dưỡng để phát triển mạnh thêm.

<!--more-->

*Do thấy thông tin trước giờ đăng toàn trong comment, khó cho người mới tìm kiếm nên thôi làm hẳn cái document cho dễ. Nội dung thì như đã từng post, nay quote lại nguyên xi:*

> Muốn phần chân với bào tử trong pore chết hết thì phải bôi thuốc, dễ mua nhất mà hiệu quả là **Nizoral**. Nhiều bạn có thể hỏi là Nizoral bôi lên người không phải lúc nào cũng khỏi thì chắc gì lên giày hiệu quả. Thứ nhất là giày ko phải  còn sống như người nên hoạt chất bôi lên ko bị đào thải đi, bôi bao nhiêu thấm ở lại trong da bấy nhiêu. Thứ 2 là lượng bôi cao hơn liều cho phép trên da người rất nhiều. Thứ 3 là đã kiểm chứng không lên lại, nếu có bị lại thì sau rất lâu (nửa năm) mà có thể do bào tử nấm mới chứ không phải nấm cũ tái phát.

{{< bimg "nizoral.jpg" >}}
<p style='text-align: center;'>*Đơn giản, hữu hiệu.*</p>

Mốc đã thay đổi cấu trúc mặt da tạo đốm rồi thì chân mốc ở dưới đã chết nó cũng không trả lại màu cũ, bắt buộc phải xi lên để cho nó che đi, đều màu lại thôi, không có cách khác. Tốt nhất là chuyển luôn sang màu nâu đậm hoặc đen để khó thấuy hơn.
Giờ ai hỏi diệt mốc như nào thì sẽ trỏ vào đây, cho nên ai muốn đóng góp ý kiến hay tranh cãi gì thì comment bên dưới.

Thân.
*VNRD - Vietnam Workwear & Denim Team*