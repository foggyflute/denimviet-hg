---
title: "Giày & bốt vỡ lòng: cấu trúc đế"
subtitle: "Khác gì nhau, đi nhiều đều mòn cả?"
date: 2018-05-23
draft: false
tags: ["Hướng Dẫn", "Giày và Bốt"]
slug: "giay bot for dummy"
image: "/post/construct/giay-bot-for-dummy/cover.jpg"
---

{{< bimg "cover.jpg" >}}

Nhận thấy có nhiều bạn không theo kịp với mọi người khi trao đổi về vấn đề giày, thứ đến nữa là có 1 số nguồn thông tin trên mạng không được chính xác (mất đi nội dung đúng khi dịch, do người truyền đạt cũng không nắm rõ hoặc có thể do người bán giày cố tình nói sai đi). Hôm nay mình ráng dành thời gian viết sơ về các cấu trúc giày thông dụng.

<!--more-->

Mặc dù khá tự tin vào hiểu biết của bản thân (mới dám đứng ra viết hướng dẫn), tuy nhiên không ai dám chắc 100% mình không sai, nên là ai thấy có gì không đúng cứ phản bác, mình sẵn sàng trao đổi/nhận sai/chỉnh sửa.

### Các thành phần của cấu trúc giày:

* **Đế trong/Insole:**  
Là thành phần trong cùng của đế (thường được che hết hoặc che nửa bằng lót giày/sockliner sau khi đóng xong). Insole bằng da tốt thì sau thời gian sử dụng lâu dài sẽ thành hình lõm theo bàn chân, đứng thoải mái. Tuy nhiên đi bộ nhiều vẫn nên nhét thêm lót giày thể thao vào phần đế vì insole da càng dùng càng cứng (nếu không bị ngâm nước đến mục).

* **Nhiễu/Welt:**  
Là 1 dải da (hoặc vật liệu khác) đi 1 vòng quanh giày (có thể dừng ở eo trước khi gặp gót giày), mục đích là làm bộ phận nối giữa mũ giày + đế trong vào với midsole hoặc outsole.

* **Nhiễu giả/Faux welt:**  
Gắn lên những loại cấu trúc giày bản chất không cần nhiễu, có cũng được, không có cũng không sao, chủ yếu là trang trí, tuy nhiên cũng có thêm 1 tác dụng là che bớt phần đáy mũ giày, làm cho thân giày nhìn mỏng bớt. Đôi lúc giày blake rapid cũng vẫn thêm faux welt vào để nhìn mũi giày mỏng lại. 

* **Độn/Filler:**  
Thường là bông ép (fiber) hoặc bần (cork), tùy độ dày, loại giày và giá thành, đôi khi còn có các loại chất liệu khác, mục đích đơn giản chỉ là lấp chỗ trống + làm phẳng bề mặt để gắn đế vào mà thôi, không có nhiều khác biệt (nếu insole tốt)

* **Đế giữa/Midsole:**  
Lớp đế nào nguyên tấm mà không trực tiếp tiếp xúc với mặt đất thì gọi là midsole, nhiều khi có thể có 2-3 midsole tùy theo kiểu giày và yêu cầu thẩm mĩ, thường là không có midsole vì khâu xuyên qua nhiều lớp thì vất vả hơn ít lớp rất nhiều.

* **Đế/Outsole:**  
Đế ngoài cùng thì gọi là outsole, bình thường chỉ gọi đơn giản là đế giày. Phần này trực tiếp tiếp xúc với mặt đất. Nếu dán thêm cái gì chồng lên trên (sau khi đã xuất xưởng) thì phần đó tính là miếng dán chứ không phải đế, nhưng nếu dán sẵn mà phủ hết mặt đế thì nó lại thành outsole, lớp bị che đi trở thành midsole.

___
### Hình minh họa các kiểu cấu trúc giày:

**Cấu trúc goodyear welt**  
Lưu ý nhỏ là nếu thấy đi mòn đứt chỉ mặt ngoài đế cũng không cần phải sợ, giữa các lớp đếvẫn có keo, và chỉ bên trong vẫn giữ đế lại được bằng sức ma sát.

{{< bimg "gyw.jpg" >}}

**Stormwelt**  
Một dạng biến tấu của goodyear, về lí thuyết thì chịu nước tốt hơn, thực tế cho thấy là... đẹp hơn.

{{< bimg "storm.jpg" >}}

**Handwelt**  
Handwelt là kiểu làm giày thủ công bằng tay hoàn toàn, điểm mạnh nhất của kiểu làm này là rất ít khoảng trống bên dưới insole, đôi khi không phải độn gì cả, đóng đế outsole còn ẩm vào là nó im và kín luôn (nhất là với kiểu flat sole digg). Nhưng khâu tay thì bao giờ cũng cực, dễ đổ máu.

{{< bimg "hand.jpg" >}}

**Stitchdown**  
Stitchdown có 2 kiểu, Viberg và Wesco dùng kiểu A. Cá nhân thì thấy kiểu B nó hơn (lí thuyết), thực tế thì không khác biệt gì khi sử dụng (độ bền).

{{< bimg "stitchdown.jpg" >}}

**Blake & blake rapid**  
Blake, kiểu khâu hay bị gắn với hình tượng sản phẩm rẻ tiền, bên dưới mình có viết đôi dòng để các bạn hiểu rõ hơn về cấu trúc này.

{{< bimg "blake.jpg" >}}

**Norvegese:**  
Thắt chỉ trang trí.

{{< bimg "norvegese.jpg" >}}

**Bologna:**  
Khâu lót thành 1 cái túi sau khi gò, rồi mới khâu blake đế vào. Nhắm đến mỏng và thoải mái.

{{< bimg "bologna.jpg" >}}

**Cement:**  
Phổ biến nhất thế giới, ta gọi bằng cái tên thân thương là 'giày đế dán', cấu trúc có hạn sử dụng: 100% sẽ đến lúc bong.

{{< bimg "cement.jpg" >}}
___
### 1 số thắc mắc / hiểu nhầm thường gặp:

**Goodyear khâu tay với khâu máy?**  
Goodyear là cái máy khâu mũ giày và insole và nhiễu, nếu khâu tay thì đã là handwelt. Có cái sự lộn xộn này là do 1 số thợ VN, Indo không rành các thuật ngữ tiếng nước ngoài, nên ngồi khâu như vậy và thấy hình sản phẩm khâu goodyear bên trong y chang nên cũng nhận là mình đang khâu goodyear, chứ thật ra đó là handwelt, và handwelt thì xịn hơn goodyear (có khâu 2 kim xỏ qua xỏ lại không sọ bung liên hoàn, cái kiểu khâu này có tên là saddle stitch).

**Stitchdown như Viberg, Wesco, White, Truman... khó làm?**  
Thật ra thì không khó, nhưng mà bị 1 cái là làm dễ bị xấu và kén loại da mũ (2 đường chỉ phải đều, đường chỉ trong phải sát, mép bẻ ra phải sắc, cạnh phần mũ giày bẻ ra không đc nhăn nheo nên phải da dày dẻo...) mà xấu thì khách không mua, thế nên là ít hãng làm stitchdown hơn làm mấy kiểu kia.

**Goodyearwelt là xịn nhất?**  
Handwelt mới là xịn nhất, khâu tay hoàn toàn, cũng là đắt nhất. Thực tế thì goodyear giá thấp (và 1 cơ số hàng giá cao nữa) thường dùng cái gân vải chứ không dùng holdfast, mà gân vải dính vào insole chỉ bằng keo thôi, nên là độ bền cũng không phải là ghê gớm gì cho lắm. Và nó cũng không hơn blake nhiều (xem phần nói về blake ngay dưới).

**Giày blake thì dở hơn goodyear?**  
Cái này không chính xác nhưng cũng không phải vô căn cứ, giày blake ngoài nhược điểm khâu xuyên vào trong nên chống nước không bằng goodyear, nhưng thực tế là giày goodyear bằng da cũng không phải chống nước giỏi giang gì cho lắm, mình đi bốt goodyear có gussetted tongue (lưỡi giày liền vào cổ giày) mà gặp mưa to hoặc lội nước thì cũng đầu hàng, căn bản da nó vẫn là thứ thấm nước (nếu không thì chân cũng không thở được, bí như đi ủng cao su). Nên lợi thế của goodyear so với blake chỉ là trên lí thuyết mà thôi.

Nhưng vì chi phí sản xuất thấp hơn goodyear do đơn giản hóa được công đoạn, nên những dòng giày giá rẻ sẽ luôn dùng blake chứ không dùng goodyear, mà giày giá rẻ thì thường cắt giảm cả về chất lượng da mũ, phụ liệu, độ tỉ mỉ cũng như kiểm soát chất lượng sẽ nới hơn. Điều này mới là lí do chính mà đại đa số giày blake trên thị trường đều nằm chiếu dưới giày goodyear.

Giày Blake-rapid thì không kém gì goodyear, tuy nhiên kiểu làm này nặng nề, tốn tiền mà dính thêm chữ blake, trong khi phong trào đi giày goodyear đang thịnh nên nó cũng không hot lắm.

**Giày xịn sẽ độn bần/cork?**  
Nếu mà xịn nhất thì sẽ như trên 1 số giày handwelt bespoke là độn 1 lớp da mỏng (đa số vẫn độn bần, nhưng mỏng). Nhưng khi làm hàng sll làm sao làm mỏng đc theo kiểu handwelt, mà độn da dày thì nặng và cứng, nên đa số chọn độn vật liệu khác: bần là 1 chất liệu phổ biến trong số đó. Nhiều nhà làm giày Anh vẫn độn nhựa đường (hắc ín) hoặc giày Pháp có nhà độn vải.

Bần thì không thấm nước, cũng lâu mủn (cứ nhìn nắp chai rượu vang thì thấy), tuy nhiên cũng không bền về mặt cơ học, đi nhiều ở trong nó cũng nát bấy. Giày giá rẻ thì hay độn mếch hoặc giấy hồng (giấy nén cứng), cũng không kém nhiều so với bần vì 2 vật liệu trên chịu nước cũng tốt, nhưng nó không xẹp đc theo thời gian đi để ôm theo lòng bàn chân như bần. Còn độn giấy cartoon thì khỏi bàn, loại này là loại siêu tiết kiệm rồi, không gọi là giày cao cấp nữa.

**Insole da khử mùi, đế da thoáng khí hơn đế cao su?**  
Insole da không khử mùi đc, nhưng nếu insole bằng da và lót cũng bằng da thì có thể thấm hút mồ hôi để giúp chân khô thoáng tương đối, chân khô thì sẽ không bốc mùi. Đế da không thoáng khí được vì giữa chân và đế da có bần, có keo, có insole nằm giữa, độ thoáng khí của đế da chả khác gì gỗ cả, nên nhận định này không có cơ sở. Nhưng da mũ và da lót thoáng và hút ẩm hơn da giả thì có, cái này ai đi giày da giả rồi sẽ biết.

**Chỉ có giày goodyear thay đế được?**  
Giày blake, blake-rapid, handwelt hay stitchdown đều thay đế được hết. Thường ta hay thấy quảng cáo giày goodyear thay đế được, đó là so sánh với giày dán (giày dán thay cũng được, nhưng nó không đẹp long lanh đc như ban đầu). Nhưng thông thường thay đế giày goodyear chi phí cũng cao, thậm chí là chi phí thay đế còn cao hơn cả 1 đôi giày dán loại tốt, cho nên yếu tố này nói chung là nghe cho sướng tai là chính, về mặt kinh tế không được hợp lí lắm. Kinh tế và hợp lí nhất vẫn là dán miếng dán lên mặt đế da, đi mòn thì thay miếng dán.

Nếu bạn ở US hay UK thì có thể đúng là không thay đc các loại kia thật, vì thợ sửa giày mấy chỗ đó mà lâu năm thường có máy rapid chứ không có máy mckay (blake) nên không nhận thay đế giày blake. Giày stitchdown thì nếu có thay đế cũng giá trên trời, nên coi như không có.

**Đế da là xịn nhất?**  
Cái này tùy theo định nghĩa xịn là gì. Đế da đi trên thảm hoặc sàn gỗ thì sướng chứ đi ngoài đường không có điểm gì gọi là hơn đế cao su cả, thua từ độ bám đến độ bền. Nhưng nhìn đôi giày đế da thì vẫn thấy sang chảnh, truyền thống, thẩm mĩ hơn, cho nên là nói xịn hơn cũng không sai.

> Trên đây là những thắc mắc và hiểu nhầm thường gặp, thỉnh thoảng sẽ cập nhập, ai có thắc mắc gì cứ email hỏi mình sẽ trả lời đầy đủ.

***Về vấn đề Copyright ảnh minh họa:** Mình có vẽ minh họa cắt ngang với mấy kiểu giày, nói chung copy đi nhớ đề nguồn là lịch sự tối thiểu, lịch sự hơn nữa thì báo mình 1 tiếng, đừng lấy rồi cắt cúp đóng logo/watermark khác vào, làm người không nên làm thế :)*

*VNRD - Vietnam Workwear & Denim Team*