---
title: "Toàn tập về fade"
subtitle: "Không phải bạc màu hay không, mà là bạc như thế nào."
date: 2019-07-12
draft: false
tags: ["Hướng Dẫn", "Denim"]
slug: "toan tap ve fade"
image: "/post/fade/toan-tap-ve-fade/cover.jpg"
---

{{< bimg "cover.jpg" >}}
<p style='text-align: center;'>*Pure Blue Japan XX-013 - Bách*</p>

```
---------------------------------------------------
+ Viết tại Sydney, 29 tháng 7, 2017.  
+ Chỉnh sửa lần cuối tại Sài Gòn 21 tháng 7, 2019.
---------------------------------------------------
```

Tác Giả: **Nguyễn Hiển** - Admin, founder Vietnam RAW Denim.  
Biên Tập: **Nghĩa Hoàng** - Admin, founder Vietnam RAW Denim.

*Sau 2 năm, cuối cùng mới hệ thống lại 1 cách đầy đủ nhất về fade, từ chỉnh lí nội dung đến hình ảnh. Hơn 2000 từ và rất nhiều hình. Mặc dù vậy, phần nội dung vẫn còn khá rối rắm, hi vọng tương lai không xa lại có thể chỉnh lí để nội dung càng dễ tiếp cận hơn cho độc giả mới làm quen với văn hóa denim nói chung và raw denim nói riêng*

<!--more-->

Chúng ta sẽ không bàn đến chuyện thích fade hay không thích fade ở đây, vì bạn thích hay không thì nó cũng xảy ra với quần của bạn, và nếu bạn không thích fade thì mình cũng chịu, đó là tùy bạn. Chúng ta cũng không bàn đến khác biệt giữa fade và wash vì đó là 2 thứ khác nhau: bạn có thể yêu thích cả cá khô và sushi, không có ai bắt ta phải lựa chọn 1 trong hai cả. Vì vậy hôm nay mình chỉ nói đến fade và một mình nó mà thôi.

Như các bạn đã biết, khi bước chân vào raw denim, tức là chúng ta đã và đang yêu thích sự tự do thể hiện cá tính của người mặc (cày), sự tinh tế của nhà sản xuất và trên hết chính là đích đến cuối cùng: faded denim – những chiếc quần được cày công phu để cho ra những mẫu wash có một không hai. Đó là cả một quá trình kiên nhẫn và không phải ai cũng “có khả năng” mặc 1 chiếc quần quanh năm suốt tháng.

Không có quần của ai fade giống của ai, bạn thậm chí có thể nhìn quần đoán người của 1 số bạn bè quen chơi jeans chung nữa. Đó là điểm độc đáo, cũng như tiêu chí xét đẹp xấu của quần jeans nói riêng và đồ denim nói chung mà các loại quần áo trang phục khác không có được.

### Fade denim là gì?

{{< bimg "fadebyhien.jpg" >}}
<p style='text-align: center;'>*Khi quần đã fade... bởi Hiển*</p>

# 'Fade'  

Là sự bạc màu, phai màu, bay màu, làm cho phai nhạt. Quá trình bạn mặc chiếc quần theo năm tháng, những nếp gấp trên chiếc quần như whiskering (bẹn), honeycombs (gối sau), stacks, train tracks, pockets....sẽ dần phai màu dần. Ở đây là màu indigo (hoặc black nếu là black denim). Nhưng denim fade như thế nào thì ta tiếp tục tìm hiểu bên dưới.

**Warp được nhuộm indigo và Weft để nguyên màu trắng**  

Quá trình bay màu này diễn ra như thế nào? Như chúng ta đã biết, denim được dệt từ 2 phần sợi cotton đan xen nhau (warp phía ngoài và weft phía trong) – warp sẽ được nhuộm indigo (màu chàm) sẵn từ trước khi dệt còn weft để trắng hoặc nhuộm xanh, chàm, đen,...(có rất nhiều màu weft tùy thuộc vào ý đồ của nhà sản xuất).

{{< bimg "redwarp.jpg" >}}
<p style='text-align: center;'>*Cá biệt khi sợi warp lõi đỏ đem nhuộm chàm, sợi weft phía trong vẫn trắng, kết quả là quần fade ra đỏ (nếu warp trắng sẽ fade ra trắng), NF Redcore - Nghĩa*</p>

**Khi màu indigo của warp phai đi, phần lõi trắng sẽ dần hiện ra** 

Thực sự là khả năng bám dính của chàm lên sợi không hề cao, nó là những phân tử màu khá to thô (hữu cơ) nên bị tác động vật lí cùng tiếp xúc với nước nhiều là nó trôi đi mất khỏi bề mặt sợi.

Đó là cái chúng ta thấy. Nhiều bạn lầm tưởng đó là màu của weft nhưng thực ra màu weft chỉ đóng vai trò bổ sung thêm tone màu đặc biệt cho quần ban đầu. Thí dụ weft được nhuộm đen hoặc indigo với ý đồ làm cho chiếc quần của chúng ta sậm hơn, đậm màu hơn, khi fade ra màu là nhân tố tạo nên electric blue fade (trắng xanh sấm chớp) của sợi warp bạc trắng trên nền màu đậm tương phản.

{{< bimg "loop.jpg" >}}
<p style='text-align: center;'>*Phai màu trên đỉa quần hiện ra màu trắng lõi chỉ - Take5 của Nghĩa*</p>

**Vậy tại sao có những chiếc quần quá trình fade diễn ra rất nhanh trong khi số khác lại chậm?**  
 
Đó là do kỹ thuật nhuộm của nhà sản xuất. Mình có từng đọc qua 1 brand khá nổi tiếng của Nhật (quên tên:D) chỉ nhuộm vừa mặt ngoài của sợi cotton tức là màu vẫn chưa ngấm vào phần lõi. Quan trọng hơn, nó còn tùy thuộc vào nhiều yếu tố khác như: khí hậu, hoạt động, ma sát tác động lên quần, thời gian mặc,...

**Ngoài kỹ thuật nhuộm ra, nguồn cotton cũng đóng vai trò khá lớn góp phần vào công cuộc fade của chúng ta.**  

Mọi người thường nghe nhiều nhất khi nhắc đến raw denim đó là Zimbabwean Cotton - đây là loại cotton dài nhất (longest staple) hiếm nhất vì đúng theo tên gọi của nó chỉ được trồng ở một vài nông trại Nam Phi và đặc biệt là thu hoạch bằng tay.

Ngoài ra còn có American Cotton (Iron Heart tin dùng), Turkish Cotton (APC, Nudie), Pima Cotton...Vì sao mình lại nhắc đến cotton? Mỗi loại cotton có khả năng hấp thụ indigo khác nhau và độ trắng của sợi cotton như đã nói ở trên ảnh hưởng rất nhiều đến kết quả fade.

Nếu có thời gian mình sẽ giới thiệu chi tiết hơn về chiếc SEXI23 (collab giữa Self-Edge và Imperial) theo như Kiya (co-founder SE) đã từng công bố:

>"...with the super white warp core equal a denim that ages with high definition in high wear areas..."

phỏng dịch:

>"... chính màu trắng tinh của lõi sợi warp là thứ tạo ra độ sắc nét của những chỗ bạc màu nhiều..."


### Phân loại các dạng fade

Trong hình là đánh dấu những vị trí chủ đạo sẽ bạc màu khi ta mặc quần jeans, đặc biệt là quần raw.

{{< bimg "fade101.jpg" >}}


&nbsp;  

##### Whiskering
 
Là phần fade ngay bẹn và túi trước của quần được tạo nên khi bạn ngồi. Đây là phần dễ xuất hiện nhất vì ai cũng phải ngồi.


{{< bimg "bazhmomo.jpg" >}}
<p style='text-align: center;'>*Wiskering electric blue vì lõi sợi trắng tinh trên quần Momotaro x TateYoko của Bách*</p>

{{< bimg "thigh.jpg" >}}
<p style='text-align: center;'>*Wiskering đùi trên quần Take5 của Nghĩa, lõi sợi màu ngà nên fade ra ngả vàng.*</p>

#### Honeycombs

Như tên gọi chính là những nếp gấp phía sau gối của bạn. Có vài người mình biết rất cá biệt, họ có khả năng cho những nếp fade từ sau gối ra đằng trước – 1 điều phi thường mà mình chưa thấy :>

{{< bimg "huytri.jpg" >}}
<p style='text-align: center;'>*Honeycomb cực tương phản trên quần N&F Elephant của Trí và jacket của Huy... tất cả bị lu mờ bởi pocket fade cờ lê trong túi sau Trí.*</p>

#### Stacks:  
Khó nhất và cũng đáng giá nhất vì phải mặc 1 cái quần dài hơn chân mình 6-8 inch. Mình đã mặc chiếc 3sixteen với L36 1 single cuff qua suốt mùa hè 36-40 độ C và kết quả cho ra rất “đã”.

Tips nhỏ giành cho những bạn thích stacks fade đó là hãy tìm 1 chiếc quần slim fit với chiều dài L dài hơn chân bạn từ 4-6 inch - thí dụ quần mình thường mặc L29 thì trong trường hợp này L34-36 là thích hợp nhất. Sau đó là nên chọn những chiếc quần trong tầm 14-18oz với độ “mềm” vừa phải, dễ fade (dark indigo càng tốt - optional).

Quan trọng nhất vẫn là sự kiên trì của “trâu”. Ví dụ như bên dưới là chiếc APC của mình (đã hem về L30) nhưng vẫn thấy được 1 ít stacks fade. Các bạn có thể tìm và xem thêm về bản collab MOTY01 của Founder bên Phần trên reddit.


{{< bimg "hien.jpg" >}}
<p style='text-align: center;'>*Stack trên quần 3sixteen ST120x (phải) và train track chạy dọc mép nối trên IH666S-od + N&F OS1 (trái) của Hiển*</p>

#### Ngoài ra còn những phần fade khác như:
___

* **Train tracks (đường tàu):** 2 mép đường selvedge in hằn ra phía ngoài quần (xem hình trên)

* **Pocket fade (túi quần):** Vật dụng để trong quần in hằn ra ngoài (xem hình trí có cây cờ lê & ví)

* **Rope fade (xoắn thừng):** chỉ xuất hiện trên những chiếc quần có chainstich hem bằng máy Union gây xoắn ống.

{{< bimg "bazhrope.jpg" >}}
<p style='text-align: center;'>*Xoắn nhăn ống trên Studio D'Artisan của Bách*</p>

* **Patch fade (tag da):** Patch nhuộm màu cũng có thể bạc ra.

{{< bimg "patch.jpg" >}}
<p style='text-align: center;'>*Fade patch trên N&F RedCore của Nghĩa*</p>


### How to fade (cày) your jawns like a boss 

Với những kinh nghiệm mặc raw từ 2012, mình sẽ chia sẽ 1 số cách trong hiểu biết của mình. Raw denim rất phù hợp với những bạn thích hoặc có công việc mang tính chất lăn lê bò lết: kỹ sư, thợ mộc, thợ máy, đầu bếp, phục vụ nhà hàng, bartender, thợ may, thợ cắt tóc, họa sĩ, nghệ sĩ đường phố,... vì kết quả cuối cùng là 1 em quần fade không thể cá tính hơn. Nudie Jeans labs có rất nhiều replica những chiếc quần fade đẹp của khách hàng mang đến sửa và đa phần những chiếc mình thích đều từ các anh họa sĩ, bartender....

Từ đó chúng ta có 2 cách cày quần raw (mình đã thử qua cả 2):
___

#### A. Clean vintage/Electric blue fade....đại loại là clean fade :>, một chiếc quần sạch sẽ.

Theo mình đây nên là điểm đến của những người mặc raw với mục đích muốn tìm hiểu và có một cái nhìn sâu hơn về những câu hỏi “vì sao”? Cách mặc rất đơn giản, có thể áp dụng cho tất cả những loại quần từ bình dân đến cao cấp: Bạn chỉ cần mặc và mặc (văn phòng ngồi máy lạnh vẫn bay nhảy thoải mái nhưng phải giặt khi quần dơ (đổ cafe, ketchup, hay trượt té vào sình lầy...).

Tất cả những sinh hoạt hằng ngày đó của bạn vẫn in dấu vào quần, có thể đậm có thể nhạt. Đôi khi giặt nhiều bay hết chàm làm thiếu đi tương phản, đôi khi vì cuộc sống là những chuỗi hành động lập đi lập lại nên tạo những đường fade rõ nét nổi bật, tất cả đều thuận theo tự nhiên mà nó tới.

Nhưng nếu bỗng chốc bạn nhận ra bản thân muốn nhiều hơn nữa từ chiếc quần, nấn ná không muốn giặt nó ngay vì nó chưa bẩn... đó là bạn đã có tư tưởng chuyển sang cách fade thứ 2 là dirty fade. 
  
#### B. Dirty fade (beater).

Đúng như tên gọi beater vốn sinh ra để bị hiếp, bị chà đạp...Với cách này thì mình cam đoan, quần của bạn sẽ fade theo 1 cách tự nhiên nhất, tinh tế nhất, nhưng dễ bị qui chụp là thằng ở dơ (gì mặc hoài 1 cái quần vậy, ở dơ quá vậy...blah). Bạn muốn giặt quần khi nào cũng được, càng dơ càng tốt, càng rách càng đẹp...

Cơ bản là không có nhiều người muốn ở bẩn, vì thế ***có 1 cách đơn giản để nhận biết quần cần giặt lúc nào (mình hay áp dụng):*** nếu phần đít quần của bạn dơ hoặc đáy quần bị toạt (giặt xong đi vá quần luôn => logic J)).

Đọc đến đây và bạn vẫn cho rằng ở Việt Nam không thích hợp cho raw denim hoặc những ý kiến xung quanh việc mặc raw là 1 thảm họa thì xin lỗi, bạn không phù hợp hoặc vẫn chưa đủ duyên để đến với raw.

Bản thân mình đã mặc suốt những chiếc raw của mình trong nhiều năm và chủ yếu giành thời gian trong vài khuôn bếp nhỏ ở Đại Lợi, lên rừng xuống biển đủ cả. Raw denim với mình gần như là must have item trong tủ quần áo (mặc dù đang là hypebeast :3)

### KẾT

>Hi vọng sau khi đọc xong 1 mớ chữ này và tiêu hóa hết, bạn nắm được tại sao vải denim lại fade, điều gì khiến nó fade đẹp fade xấu khác nhau, fade gồm có những gì trên mỗi chiếc quần, và cuối cùng cách mặc fade nào là lựa chọn của bạn. Chúc các bạn lựa chọn được chiếc quần đúng gu để fade ra thật hợp ý. 

*Bài viết được dựa trên 1 số nguồn tham khảo từ Reddit, Heddels, Superdenim, nhiều nguồn khác và trên hết là kinh nghiệm cá nhân của tác giả (Hiển). Nếu bạn đọc có góp ý hoặc thảo luận quanh vấn đề này vui lòng email ! Thân !*

*VNRD - Vietnam Workwear & Denim Team*