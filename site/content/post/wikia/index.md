---
title: "Denim: Thuật Ngữ & Khái Niệm"
subtitle: "Update 1.0.3 Formating"
date: 2018-05-08
draft: false
tags: ["Hướng Dẫn", "Denim"]
slug: "denim wikia"
image: "/post/wikia/denim-wikia/cover.jpg"
---

{{< bimg "cover.jpg" >}}
___

### UPDATE LOG

```
---
- Version 1.0.3 Format, thêm ảnh minh họa, chuyển qua định dạng markdown trên website.
- Version 1.0.2 bổ sung định nghĩa mill và slub
- Version 1.0.1 formating
- Version 1.0
---

```

Thông tin không tránh khỏi có chỗ sai lệch, ai thấy nội dung nào sai hãy thông báo để đính chính/bổ sung cho cộng đông. Xin cảm ơn!

<!--more-->
___
### DENIM

Tiếng Việt gọi là 'vải bò' - trong quần bò, áo bò. Là loại vải may bằng sợi bông (cotton) may bằng 2 lớp sợi đan chéo nhau (twill), thông thường là theo dạng sợi ngang nhảy qua 3 sợi dọc, gọi là 3x1:

{{< bimg "3x1.jpg" >}}
<p style='text-align: center;'>*Nguồn ảnh: Heddels*</p>

Lớp chỉ (yarn) chạy xéo là warp lộ lên mặt trên đc nhuộm màu, lớp chỉ chạy ngang bên dưới là weft thường để trắng (nếu quần lộn ra bên trong có màu là weft cũng nhuộm).

**Thành phần của denim:**

Vải denim có thể 100% cotton (thường gặp) hoặc pha spandex (co dãn cho quần bó), pha kevlar (chống trầy cho quần biker - dân chạy xe máy) hoặc nhiều loại chất liệu khác nhưng không thông dụng. Cotton làm denim thì theo cộng đồng dân chơi thế giới xếp cotton Zimbabwe xịn nhất, xong đến Turkish (Thổ Nhĩ Kỳ), thông thường là Pima - giống cotton của Trung Mĩ giờ trồng khắp nơi.

{{< bimg "zw.jpg" >}}
<p style='text-align: center;'>*Nguồn ảnh: Heddels*</p>

**Độ dày của denim:**

Độ dày của vải denim thường tính theo cách lấy 'cân nặng/đơn vị diện tích', vì nhiều khi vải bông xốp đo dày theo mm nhưng vẫn mỏng thoáng, mà vải bố nhìn mỏng nhưng lại là dà đặc không xuyên kim qua nổi. Hệ đo lường imperal (Anh và Mĩ dùng, khác với hệ SI vn dùng) thì dùng ounce/square yard (oz/yd², gọi tắt luôn là oz. nếu đã biết đang nói về vải) trong khi hệ SI (quốc tế và VN dùng) thì hay dùng gram/quare metter (g/m² hoặc gsm).

*Một số thông tin độ dày vải cho các bạn dễ hình dung.*
___

Ứng dụng | Độ Dày
--- | ---
Vải bò may áo sơ mi đứng áo | 7-9oz
Quần skinny nữ có pha thun | 9-10oz
Quần skinny nam có pha thun | 10-12oz
Quần bò thông thường | 12.5-14.5oz
Quần tương đối nặng | 15-17oz
Quần nặng | 18-21oz
Quần cho bọn khổ dâm | 23-33oz
___

*Lưu ý là số này tương đối, nhiều khi có quần 16.5oz cứng mặc đau đít hơn 18.5oz là bình thường, hoặc cùng 1 loại vải nhưng 2 brand khác nhau 1 thằng báo 21oz 1 thằng báo 23oz cũng là bình thường.*
___
### INDIGO

Tiếng Việt gọi là 'chàm' - trong nhuộm chàm, tay nhúng chàm. Là màu nhuộm cơ bản của đồ denim, là loại màu nhuộm hữu cơ ban đầu chiết xuất từ thực vật, hiện đa số là tổng hợp. Indigo mang lại màu xanh đặc trưng rất khó để dùng màu nhuộm hóa học tạo ra (với giá rẻ hơn). Và hay nhất là nó khá dễ phai màu, dẫn đến cái mà ta gọi là 'fade' sau một thời gian sử dụng.

&nbsp;  
{{< bimg "indigo.jpg" >}}
<p style='text-align: center;'>*Bột màu chàm đang trong sản xuất.*</p>
___
### RAW DENIM

Raw ở đây là 'sống' - chưa xử lí, kiểu như trong cá sống thịt sống rau sống, khâu may xong như nào là để nguyên như vậy.

&nbsp;  
#### Tạo nên raw denim có 3 yếu tố:
___

1. **Vải denim nhuộm từ sợi ra thường sẽ đậm (gần như đen)**, nhuộm bằng indigo nên dễ phai màu, điều này không phải ai cũng khoái (thậm chí còn nhiều người bán quần quảng cáo hàng xịn không phai màu) nên sau khi may xong người ta sẽ giặt cho phai bớt và có thể cả xử lí cho nó ít phai màu bằng hóa chất chốt màu.


2. **Vải denim cuộn đc hồ cứng (starch)** để dễ thao tác khâu may chính xác cho công nhân may hơn là vải mềm, sau khi may xong ngưuời ta thường sẽ giặt cho hết cái hồ cứng này đi.


3. **Sau khi may xong quần có thể đc xử lí** bằng axit, laser, bắn bột đá, mài, enzime, vân vân... để tạo hiệu ứng.

Nếu 1 cái quần khi may xong KHÔNG trải qua cả 3 yếu tố trên, tức là KHÔNG xử lí gì hết, cứ thế bỏ vào túi đem bán thì gọi là quần RAW, tức là nó còn sống nhăn. Và chúng ta đang trả thêm tiền để mua cái quần trải qua ít công đoạn hơn với giá cao hơn bình thường, xét theo lẽ thường thì như thế là ngu, nhưng nói ra sẽ bị BQL group ghi vào sổ thù vặt ngay, khuyên các bạn không nên :))

{{< bimg "raw.jpg" >}}
<p style='text-align: center;'>*Quần vải "raw"*</p>
&nbsp;

#### *Vậy lí do gì để mua quần raw thay cho quần wash?*
___

1. **Chất lượng:** thời đại hỗn mang, vải bò thượng vàng hạ cám, 1 cái quần bằng vải raw cũng là 1 tiêu chí để lọc bớt hàng tạp nham. Hơn nữa nếu bạn muốn mua quần dày, bền thì ngoài quần raw ra cũng khó có lựa chọn khác.

2. **Độ bền:** Quần dày hơn thì bền hơn, té đỡ rách (trong vẫn trầy trụa bầm tím nhưng quần quan trọng hơn da thịt xương cốt nhé /s), mặc đã hơn, muốn có quần dày thì phải mua raw, chứ ra cửa hàng thời trang thường không có quần dày hơn 13.5oz mấy đâu. Quá trình wash tuy đẹp nhưng cũng làm mòn yếu bớt những sợi chỉ, quần raw thì không trải qua cái này nên chỉ của vải luôn ở trạng thái tốt nhất khi ta mua về.

3. **Cá nhân hóa:** Quần raw chưa tẩy đi lớp hồ cứng nên khi mặc vào sẽ gãy hằn theo kiểu riêng khít theo người của chủ cái quần, những đường hằn gãy này mỗi người khác nhau + dáng quần khác nhau sẽ ra khác nhau, không ai giống ai, có thể nói là riêng mình.

4. **Quần wash sẵn nhiều khi cũng nhìn rất đẹp, nhưng yêu cầu người mặc hơi khắt khe:** không đủ cao thì phần mài đùi bạc màu sẽ kéo xuống quá gối, phần mài nhăn nhăn ở ngay bắp vế, ống rách đẹp bụi bặm thì đùn 1 cục dưới mắt cá. Nhiều khi chiều cao đủ nhưng mặc vô đường nhăn ngay háng với đường mài sẵn của quần chả liên quan gì đến nhau. Nói chung thì đôi phần tạo cảm giác không phải là quần của mình. Có lẽ đa số không để ý đâu (sau khi đọc cái này nhìn lại quần wash đang mặc mà bị khó chịu với các chi tiết này thì báo để QBL biết mà cười trên sự đau lòng của bạn).

5. **Cảm hứng:** Suy cho cùng những cái quần wash sẵn đẹp tự nhiên (không tính đến mấy quả laser độc độc hoặc vẩy sơn quái dị) đa số đều lấy cảm hứng từ những cái quần đã được mặc và bạc màu theo cách truyền thống (bạc đùi, rách gấu, rách gối, thủng lỗ, nhăn khoeo,...) Vậy tại sao mình không tự làm bạc quần của mình. Khối ông thợ hồ, thợ sơn quần siđa quần chợ, mặc đẹp nhìn chảy dãi luôn, vì nó là fade thật!

&nbsp;  
{{< bimg "fade.jpg" >}}
<p style='text-align: center;'>*Chiếc quần đã được 'cá nhân hóa' thông qua fade của Hiển - admmin Vietnam RAW Denim.*</p>
&nbsp;
___
### SANFORIZED & UNSANFORIZED

Ngoài ra bạn có thể nghe nói đến khái niệm sanforized với unsanforized, sanforize không có từ tiếng Việt tương đương nên mình không dịch, nó là cho vải chạy qua 1 cái máy có nhiều ống kim loại cuốn căng, xông hơi nước nóng (steam) để cho vải co dãn đưa đến độ co rút tối đa --> sau khi may xong giặt đi nó không co lại nữa. Đồ raw bằng vải denim chưa qua quá trình này (unsanforized) sẽ rút lại nửa size đến 1 size khi giặt lần đầu, vải đã qua quá trình này thì sẽ co rút rất ít, không đáng kể. Sanforized không tính là đã wash vì vải sanforized màu vẫn đậm, bán cho bên may vẫn có hồ cứng.

{{< bimg "san.jpg" >}}
<p style='text-align: center;'>*Nguồn ảnh: cibitex.it*</p>

___
### WASH DENIM

Wash là đã xử lí, không chỉ nói đến giặt mà còn là 1 tổ hợp rất nhiều quá trình để tạo nên màu sắc hoa văn họa tiết, cả sờn rách khâu vá theo như mong muốn của nhà thiết kế. Mẫu wash có thể làm giả vờ như quần đã bạc màu sau khi mặc lâu, hoặc có thể hoàn toàn sáng tạo, hoặc kết hợp cả 2.

&nbsp;  
#### 1 số phương pháp wash phổ biến:
___

* **Wash đá:** dùng súng phun bột đá để mài mòn vải, tạo nên bạc màu

* **Wash acid:** cho quần lên manercanh co lại cho nhăn như người mặc, cho vào hấp acid để những đường nhăn lồi tiếp xúc với acid nhiều hơn sẽ bạc màu

* **Wash enzime:** dùng enzime để ăn mòn - ai biết chi tiết bổ sung hộ

* **Wash rách:** dùng đầu máy mài đánh lên các vị trí mong muốn để phá sờn rách vải, có thể vá lại hoặc không. Đội khi phần vá/mạng/khâu mới là tâm điểm chứ không phải rách sờn.

* **Wash laser:** cho lên máy bắn laser bắn cho cháy bạc vải theo hình dạng mong muốn.

* **Wash "dơ":** vẩy sơn, quệt màu, sáp, keo lên để tạo thành các vết tích trên vải.

* **Wash phủ ?!?:** phủ vải bằng chất chống thấm, sáp, bột kim tuyến, phản UV, phản quang...

&nbsp;  
{{< bimg "wash.jpg" >}}
<p style='text-align: center;'>*Nguồn: chụp tại Copper Denim*</p>

Mình không chuyên về wash denim, ai rành viết hẳn 1 bài thì rất hoan nghênh.

___
### FIT

Fit ở đây là vừa khít: mỗi người có cơ thể khác nhau, người béo người gầy, người bụng to người mông to có người lại bắp chân to. Cho nên các hãng có nhiều loại fit khác nhau dể phù hợp với nhiều kiểu cơ thể và sở thích khác nhau (người thích thoải mái người lại thích bó bó cho sexy).

&nbsp;  
#### Các từ ngữ chuyên về fit:
___

* **Straigth:** ống quần may thẳng từ gối xuống, ống này to, phù hợp chân ai to bắp vế hoặc thích thoải mái, vintage.

* **Tapper/tappered:** từ bắp vế hơi ôm thêm vào 1 tí để chỗ gấu quần nó nhỏ lại.

* **Slim:** bóp từ đùi ôm vào gối và bắp vế, ôm thêm vào gấu.

* **Skinny:** bó từ đùi xuống sát theo chân xuống tới ống quần ôm chặt vào cổ chân.

* **Carrot:** trên đùi thì lùng bùng phồng ra, từ gối xuống thì bóp như slim hoặc skinny.

* **Drop crotch:** đũng quần thấp, hay còn gọi là đít xệ.

Các hãng sinh ra thêm đủ loại fit khác nữa lai giữa các fit trên hoặc cùng skinny nhưng độ ôm bó khác nhau tùy theo hãng và loại vải, nên tham khảo kĩ số đo và hình ảnh trước khi chọn fit, thử được thì tốt nhất.

___
### JACKET TYPE

&nbsp;
{{< bimg "type.jpg" >}}
<p style='text-align: center;'>*Nguồn ảnh: Heddels*</p>

Sẽ dịch 1 bài về jacket sau, giờ chưa đủ thời gian. Tạm thời tóm gọn lại như sau:
___

* **Type 3** là loại phổ biến nhất, trang trí 2 chữ V 2 bên thân, túi áo cao trên ngực.

* **Lee rider** cũng khá phổ biến, túi áo cao tròn mép, trang trí 2 gân dọc mỗi bên thân, có đường zig zag ngay vạt cài cúc.

* **Type 2** là loại áo có 2 đường xếp li dằn xuống bằng nhiều đường khâu hình chữ nhật mỗi bên thân, 2 túi áo thấp ngay sườn.

* **Type 1** giống type 2 nhưng chỉ có 1 bên túi áo thôi.  


{{< bimg "rider.jpg" >}}
<p style='text-align: center;'>*Lee rider... nhưng do IH làm*</p>

### NHỮNG TỪ THƯỜNG GẶP
___

- **selvedge/self-edge:** là biên vải, mép vải mà con thoi chỉ tự lộn vô, không cắt hoặc thả nên không có tưa, thường thấy ở mép ngoài quần raw, hiện nay phổ biến cả ở 1 số quần wash. Chỉ có quần áo may bằng vải khổ nhỏ 0.6m đến 1m trên máy truyền thống mới có mép này, vì vải công nghiệp khổ lớn không có mép này hoặc có đi nữa cũng không đủ để dùng mép vải trên toàn bộ quần may ra từ cây vải đó.

- **wishkers fade:** "râu mèo" chỉ đường vân phai bạc màu ngay háng kéo ra 2 bên như râu mèo

- **traintrack, rail road fade:** chỉ đường hằn lên ở mép gấp nối bạc thành từng sọc ngang nhìn như đường tàu

- **honeycomb fade:** "tổ ong" ở khoeo tay khoeo chân hoặc gấu, nói chung là chỗ bị đùn lại thành 1 đống sẽ lên fade này

- **blow out (crotch):** mặc quần chật mà thích ngồi xổm hoặc dạng chân sẽ rách ngay háng

- **cuff:** xắn quần nói là để cho gọn do quần dài nhưng thường là để khoe selvedge mà thôi.

- **buttoned:** quần dùng cúc chứ không dùng zip, sẽ không bị đùn 1 cục khi ngồi nhìn như rối loạn cương dương

- **rivet:** mấy cái khuy đồng đính ở mép túi để chống rách

- **bar tack:** con bọ, đính bọ cái này không biết giải thích thế nào, bạn google cái là thấy, mục đích cũng là chống rách.

- **chain stitch:** đường may móc xích, mặt ngoài nhìn như đường may thường nhưng lộn mặt trong sẽ thấy đường chỉ móc xích, chủ yếu cho đẹp, truyền thống vì may thường chắc hơn

- **overlock:** vắt sổ

- **slub:** vải nổi cục cục sợi weft lên trên bề mặt warp, nhìn xa như tuyết bám, nhìn gần giống như bông li ti nổi lên. Kiểu vải này khá thịnh hành ở Nhật.

- **mill:** xưởng dệt, công ty dệt.

Bổ sung hình ảnh và nội dung ngày 12/7/2019.
*VNRD - Vietnam Workwear & Denim Team*