---
title: "Cách bóp ống quần selvedge tiêu chuẩn."
subtitle: "Cái khó là làm sao mô tả cho thợ hiểu"
date: 2017-07-23
draft: false
tags: ["Hướng Dẫn", "Denim"]
slug: "huong dan bop ong giu selv"
image: "/post/bopong/huong-dan-bop-ong-giu-selv/cover.jpg"
---

{{< bimg "cover.jpg" >}}

Thấy anh em có vẻ mò mẫm và nhiều khi bị thợ lười thợ đểu nó chỉ bậy, mình sẽ trình bày sơ lược về cách bóp ống cho quần selvedge như thế nào là chấp nhận đc. Xét về mặt kĩ thuật thì cực kì đơn giản, có điều nhiều khi thợ chưa đụng vô quần selvedge bao giờ nên không hiểu được mình muốn làm cái gì.

<!--more-->

**Tiêu chí bóp ống là:**  

* Bóp nhỏ đc cả ống và gối (bóp chỉ có ống không nó tapper vào rất kì cục, hỏng dáng).

* Giữ nguyên selvedge không động vào, đường bóp chắc chắn không sợ bị bung, vắt sổ kĩ.

* KHÔNG cần giữ lại đường chỉ móc xích nếu giữ lại thì ngay chỗ đường khâu lại nó cũng bùi nhùi + làm mất công hơn không đáng.

**Cách bóp (nói với thợ là họ hiểu):**

>Bóp cho em ống còn xx cm, cắt bớt ở phía trong - chỗ có đường vắt sổ từ đáy/ đít quần xuống đến ống, phía ngoài chỗ biên vải không đụng vô. Dỡ hết vắt sổ cũ, khâu với vắt sổ lại luôn.

{{< bimg "1.jpg" >}}
<p style='text-align: center;'>*Vắt sổ mới sau khi cắt bỏ (ống 20 xuống 17, bóp cả gối và lượn ra dần đến gần háng).*</p>

{{< bimg "2.jpg" >}}
<p style='text-align: center;'>*Cuối đường vắt sổ mới và đường vắt sổ cũ còn lại ở gần háng.*</p>

{{< bimg "3.jpg" >}}
<p style='text-align: center;'>*Selvedge phía ngoài không bị đụng đến. Nói không với selvedge béo nha :)*</p>

{{< bimg "4.jpg" >}}
<p style='text-align: center;'>*Xong!*</p>

Ở Sài Gòn có thể liên hệ Akito trong group, hoặc nhà may của nhà bạn Trí Phạm, hoặc tiện tay qua Copper Denim mua hàng rồi nhờ vả luôn. Trên thực tế đã sửa nhiều quần ở nhiều nơi, nói người ta hiểu là đc, cũng không phải khó khăn gì. Những chỗ trên thì đã quen, không sợ dặn rồi người ta quên thôi.

Anh em Hà Nội sửa chỗ nào ưng ý đạt chuẩn rồi thì chia sẻ để người khác đỡ mất công thuyết trình lại.

**Quần trong hình:** American Eagle, ống 20 bóp xuống 17, thực hiện tại tiệm Cường @ Lý Chính Thắng nhưng nghe nói thợ cũ tiệm Cường đã nghỉ, có 1 bạn bóp bị làm mất selvedge, không biết có được đền hay không, thông báo đến anh em để né không bị đau khổ.

Chúc may mắn.

*VNRD - Vietnam Workwear & Denim Team*