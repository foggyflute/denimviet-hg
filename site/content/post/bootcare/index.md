---
title: "Hướng dẫn chăm sóc boots từng bước 1 cách đơn giản."
subtitle: "Chăm sóc cẩn thận để hành hạ được lâu."
date: 2018-01-04
draft: false
tags: ["Hướng Dẫn", "Giày và bốt"]
slug: "cham soc bot"
image: "/post/bootcare/cham-soc-bot/cover.jpg"
---

{{< bimg "cover.jpg" >}}
___

**Lưu ý 1:**  
Có cực kì nhiều những hướng dẫn khác ở trên mạng, nhiều cái chi tiết hơn cái này nhiều, kết quả cũng xịn hơn nhiều. Nếu mà bạn đã rành rồi thì không cần đọc cái này đâu. Mà nếu nhỡ đọc và thấy chưa đúng thì góp ý mình nhẹ nhàng, mình mỏng manh dễ vỡ lắm :'(

<!--more-->

**Lưu ý 2:**  
Đây là hướng dẫn chăm boots đi cày. Mình chăm giày tây với dress boots kiểu khác, đánh bóng rồi buff màu này nọ, với cả giày tây đi cho nó sang chảnh thì không bị hành hạ như này, nên cách chăm cũng khác. Cái này là cơ bản thôi, người khác làm cách khác cũng vẫn đúng.

**Lưu ý 3:**  
Trong lúc đánh kem dưỡng và xi thì có dùng bàn chải lông ngựa, mà chụp xong mới để ý là không thấy có hình, nhưng các bạn nhớ kiếm bàn chải lông ngựa nhé :))

{{< bimg "1.jpg" >}}
<p style='text-align: center;'>*Xấu bẩn công trường.*</p>

### Các bước thực hiện:

Đầu tiên là gỡ dây ra, cái lưỡi giày ở dưới cái dây là 1 trong những chỗ nhiều bụi bẩn nhất, không nên lười.

{{< bimg "2.jpg" >}}

Bạn có thể dùng bàn chải lông ngựa đánh bụi đất cũng đc, nhưng mà mình ngại bẩn bàn chải nên thường là lấy giẻ ẩm ẩm lau, lau kĩ nhất là mấy chỗ mép nối, bụi bẩn hay bám mấy chỗ này.

{{< bimg "3.jpg" >}}

Xong dùng bàn chải đánh răng loại mềm để chải cho hết bụi ở mấy khe rãnh, xong lau lại nếu cần. Phải chùi kĩ, bởi bụi bẩn lẫn vào kem dưỡng hay xi thì trông gớm lắm. Đôi này đế cao su, mà mai lại đi nên chả thèm đánh đất bẩn khỏi đế, chứ đế cũng nên sạch sẽ tí, lau chùi cả viền đế luôn.

{{< bimg "4.jpg" >}}

Cho tí Lexol lên để dưỡng, vì nó rẻ và hiệu quả, dưỡng lexol xong sờ rất là thích. Nhưng không bắt buộc, nếu không có thì bước tiếp theo dùng nhiều kem dưỡng lên 1 tí.

{{< bimg "5.jpg" >}}

Dưỡng xong thì thường mấy tiếng đến 2 ngày mới khô, nếu chưa cần đi gấp thì để đến hôm sau làm tiếp, không thì dưỡng ít ít thôi, đừng có đổ ướt cả giày. Lexol gốc nước nên là nếu dưỡng xong đánh xi/kem liền nó không bóng đc, khó thấm vào da, lại ngăn cản lexol bay hơi, nói chung là không tốt cho da.

{{< bimg "6.jpg" >}}
<p style='text-align: center;'>*sau khi dùng lexol và để chờ khô xong.*</p>

Nếu lót gỡ đc thì gỡ ra để cho khô ráo, cái này hút ẩm từ chân nhiều nên dễ mốc --> mốc, lây mốc cho cả giày. Nếu thấy khô cứng quá thì cho nó tí dầu neatsfoot, vẫn mềm đanh thì không cần.

{{< bimg "7.jpg" >}}
<p style='text-align: center;'>*có thấy hình bàn chân ko kìa...*</p>

Đánh kem dưỡng lên. Nếu có lexol thì không cần nhiều vì 2 cái đều là dưỡng. Nhưng mà kem thì nó có dầu nên nó làm giày bóng trên mặt + nếu chỗ nào bị cạ đổi màu, màu không đều thì nó làm đều màu lại. Nếu đánh cùng màu da hoặc đâm hơn màu da 1 tí với mấy màu khác ngoài đen thì da trông có chiều sâu + nổi bật hơn. Dùng màu sáng hơn lên da màu tối hơn không sao hết (trường hợp cùng màu khác sắc độ đậm nhạt). Đã dùng Burgol, Woly, Saphir thấy cả 3 đều tốt, không rõ cái nào tốt hơn cái nào.

{{< bimg "8.jpg" >}}

Cách đánh là lấy ngón tay phết mỏng nhẹ lên mặt da giày, như bôi nivea lên mặt người yêu. CHỜ cho nó khô bớt khoảng 10-15p rồi mới lấy bàn chải lông ngựa đánh nhanh nhưng nhẹ nhàng (lên youtube xem người ta cầm như nào). Đánh sớm quá nó dính vào bàn chải phí kem chả đc gì. Sau khi đánh bóng bằng bàn chải, lấy vải thun láng mịn đánh thêm nếu thích, nó sẽ bóng hơn 1 tí.

Nếu mà không thích bóng thì tới đây ok rồi, nhưng mà nếu có xi thì nó che đc cả vết xước, vết cắt, tội gì không dùng.

{{< bimg "9.jpg" >}}
<p style='text-align: center;'>*Mới kem dưỡng thôi, chưa có xi nó đã hơi bóng bóng.*</p>

Đây là xi, nứt nẻ, khô tí cũng không sao nhé. Cách dùng xi thì cũng y chang như cách dùng kem dưỡng (dùng ngón tay, chờ, dùng bàn chải rồi giẻ).

{{< bimg "10.jpg" >}}

Xi màu đen thì thấy dùng hãng nào cũng đc, kể cả kiwi. Với giày màu sáng, giày patina, thích bóng soi gương thì nên mua xi saphir xịn, mua thêm cái mirror gloss thì đánh mũi giày càng nhanh soi gương.

{{< bimg "11.jpg" >}}
<p style='text-align: center;'>*Giày đi cày nên chỉ đánh bóng đến cỡ này thôi.*</p>

Nếu thấy đế da với nhiễu giày nó khô quá thì cho nó tí dầu neatsfoot, nhưng đôi này thì chưa cần.

{{< bimg "12.jpg" >}}
<p style='text-align: center;'>*Xỏ dây vào là mai lại ra đường bụi bẩn :)*</p>


*VNRD - Vietnam Workwear & Denim Team*