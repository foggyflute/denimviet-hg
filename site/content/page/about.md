---
title: "Giới Thiệu"
subtitle: "Mặc quần jeans, đi bốt."
comments: false
---

Workwear là 1 phong cách mà cái đẹp đi từ cuộc sống ra, những món đồ được thiết kế ra dưới nhu cầu của người lao động để rồi từ đó trở thành một dạng văn hóa ăn mặc độc đáo vượt thời gian.

Trong các nhánh phát triển của Workwear thì denim & jeans luôn là những món đồ dẫn đầu về sức hấp dẫn bởi sự đa dạng và tiện dụng cũng như độ bền vượt trội so với đa số các loại quần áo khác suốt cả thế kỉ. Màu nhuộm chàm indigo tạo nên những biến tấu vô tận về sắc độ làm bao nhiêu người đam mê tìm hiểu và sưu tập chưa hề có dấu hiệu nguội lại bao giờ.

Chúng tôi mong muốn thông qua website này, có thể mang đến cho độc giả những thông tin bổ ích giá trị, những tin tức mới nhất trên thị trường thế giới: Tiến tới đưa cộng đồng yêu thích đồ denim, workwear và vintage phát triển ngang tầm với bạn bè trong khu vực với số lượng người tham gia ngày càng đông đảo hơn.

___

#### Chịu trách nhiệm nội dung: BQL cuả group Vietnam RAW Denim.

* FB Page: [Vietnam Workwear & Denim](https://www.facebook.com/VietnamWorkwearAndDenim/)
* Group: [Vietnam RAW Denim](https://www.facebook.com/groups/VietnamRAWdenim/)